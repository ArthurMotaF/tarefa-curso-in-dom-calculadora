let notas = []

let tabela = document.querySelector(".notasSalvas")

let posicaonota = 1

// btn para add notas 

let botaoAdicionar = document.querySelector(".adicionar")
adicionar.addEventListener("click", function (adicionarNota) {

    let input = document.querySelector("#nota")

    input.value = trocarVirgula(input.value)
    let validade = testeVerificação(input.value)

    if (validade == true) {
        let texto = document.createElement("option")
        texto.innerText = `A nota ${posicaonota} foi ${input.value}`
        notas.push(parseFloat(input.value))
        tabela.append(texto)
        input.value = ""
        posicaonota++
    }
    else {
    }
});

function trocarVirgula(notaInserida) {
    if ((notaInserida.match(/,/g) || []).length == 1) {
        notaFormatada = notaInserida.replace(",", ".")

        return notaFormatada
    }

    return notaInserida
}

function testeVerificação(notaInserida) {

    if (notaInserida == "") {
        alert("Insira uma nota não vazia")
        return false
    }
    
    else if (notaInserida < 0 || 10 < notaInserida) {
        alert("São válidas apenas notas entre 1 e 10, por favor, insira uma nota válida.")
        return false
    }
    
    else if (isNaN(parseFloat(notaInserida))) {
        alert("São válidas apenas notas númericas, por favor, insira uma nota válida.")
        return false
    }
    
    return true
}

// Btn para calcular

let button_calcular = document.querySelector(".calcular_media")
button_calcular.addEventListener("click", function (calcularMedia) {

    let soma = 0
    notas.forEach((nota) => {
        soma += nota
    })

    let media = soma / notas.length

    let mostrarMedia = document.querySelector(".media")
    mostrarMedia.innerText = `A média é: ${media.toFixed(2)}`

});